using UnityEngine;

namespace Massive.Gameplay.Components
{
    public class EnemySpawnPoint
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public float LastSpawnTime;
    }}