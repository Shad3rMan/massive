using UnityEngine;

namespace Massive.Gameplay.Components
{
    public class Trajectory
    {
        public Vector3[] Points;
        public Vector3 CurrentTarget;
    }
}