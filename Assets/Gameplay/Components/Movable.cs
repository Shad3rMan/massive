using UnityEngine;

namespace Massive.Gameplay.Components
{
    public class Movable
    {
        public Rigidbody Rigidbody;
        public Vector3 Position;
        public Vector3 Direction;
        public float Speed;
    }
}