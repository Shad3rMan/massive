﻿using Infra.Controllers;

namespace Massive.Gameplay
{
    public class TestController : BaseController
    {
        public TestController(IControllerFactory controllerFactory) : base(controllerFactory)
        {
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override bool HandleMessage(BaseMessage message)
        {
            return false;
        }
    }
}