using System.Threading.Tasks;
using UnityEngine;

namespace Massive.Gameplay.GameResources
{
    public class ResourcesProvider : IResourceProvider
    {
        public T GetResource<T>(string path) where T : UnityEngine.Object
        {
            return Resources.Load<T>(path);
        }
    }
}