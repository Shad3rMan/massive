using System.Threading.Tasks;

namespace Massive.Gameplay.GameResources
{
    public interface IResourceProvider
    {
        T GetResource<T>(string path) where T : UnityEngine.Object; 
    }
}