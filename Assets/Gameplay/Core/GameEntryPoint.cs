using Leopotam.Ecs;
using Massive.Gameplay.Core.GameInitialization;
using UnityEngine;

namespace Massive.Gameplay.Core
{
    public class GameEntryPoint : MonoBehaviour
    {
        private EcsWorld _world;

        private EcsSystems _gameplaySystems;
        private EcsSystems _physicsSystems;
        private EcsSystems _gizmoSystems;

        private void Start()
        {
            var gameInitializer = new GameInitializer();
            gameInitializer.Initialize();
        }

        private void OnEnable()
        {
            _world = new EcsWorld();
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
#endif
            _gameplaySystems = new EcsSystems(_world);
            var gameplaySystemsInitializer = new GameplaySystemsInitializer(_gameplaySystems);
            gameplaySystemsInitializer.Initialize();

            _physicsSystems = new EcsSystems(_world);
            var physicsSystemsInitializer = new PhysicsSystemsInitializer(_physicsSystems);
            physicsSystemsInitializer.Initialize();

            _gizmoSystems = new EcsSystems(_world);
            var gizmosSystemsInitializer = new GizmoSystemsInitializer(_gizmoSystems);
            gizmosSystemsInitializer.Initialize();

#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_gameplaySystems, "GameplaySystems");
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_physicsSystems, "PhysicsSystems");
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_gizmoSystems, "GizmosSystems");
#endif
        }

        private void Update()
        {
            _gameplaySystems.Run();
        }

        private void FixedUpdate()
        {
            _physicsSystems.Run();
        }

        private void OnDrawGizmos()
        {
            _gizmoSystems?.Run();
        }

        private void OnDisable()
        {
            _gameplaySystems.Dispose();
            _physicsSystems.Dispose();
        }
    }
}