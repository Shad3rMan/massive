﻿using System.Collections.Generic;

namespace Massive.Gameplay.Core.GameInitialization
{
    public class GameInitializer
    {
        private readonly List<IInitializer> _initializers;

        public GameInitializer()
        {
            _initializers = new List<IInitializer>
            {
                
            };
        }

        public void Initialize()
        {
            foreach (var initializer in _initializers)
            {
                initializer.Initialize();
            }
        }
    }
}
