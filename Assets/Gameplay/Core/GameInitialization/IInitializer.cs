﻿namespace Massive.Gameplay.Core.GameInitialization
{
    public interface IInitializer
    {
        void Initialize();
    }
}