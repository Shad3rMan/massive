using Leopotam.Ecs;

namespace Massive.Gameplay.Core.GameInitialization
{
    public abstract class SystemsInitializer : IInitializer
    {
        private readonly EcsSystems _systems;

        protected SystemsInitializer(EcsSystems systems)
        {
            _systems = systems;
        }

        protected void AddSystem(IEcsSystem system)
        {
            _systems.Add(system);
        }
        
        public void Initialize()
        {
            _systems.Initialize();
        }
    }
}