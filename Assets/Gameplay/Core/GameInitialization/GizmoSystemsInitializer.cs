using Leopotam.Ecs;
using Massive.Gameplay.Systems;

namespace Massive.Gameplay.Core.GameInitialization
{
    public class GizmoSystemsInitializer : SystemsInitializer
    {
        public GizmoSystemsInitializer(EcsSystems systems) : base(systems)
        {
            AddSystem(new TrajectoryGizmosSystem());
        }
    }
}