using Leopotam.Ecs;
using Massive.Gameplay.Systems;

namespace Massive.Gameplay.Core.GameInitialization
{
    public class PhysicsSystemsInitializer : SystemsInitializer
    {
        private readonly EcsSystems _systems;

        public PhysicsSystemsInitializer(EcsSystems systems) : base(systems)
        {
            AddSystem(new MoveSystem());
        }
    }
}