using Leopotam.Ecs;
using Massive.Gameplay.GameResources;
using Massive.Gameplay.Systems;

namespace Massive.Gameplay.Core.GameInitialization
{
    public class GameplaySystemsInitializer : SystemsInitializer
    {
        private readonly EcsSystems _gameplaySystems;

        public GameplaySystemsInitializer(EcsSystems gameplaySystems) : base(gameplaySystems)
        {
            AddSystem(new EnemySpawnPointsInitSystem());
            AddSystem(new PlayerInitSystem());
            AddSystem(new SpawnSystem(new ResourcesProvider()));
            AddSystem(new DirectionSystem());
            AddSystem(new SpeedSystem());
            AddSystem(new MovableOrientationSystem());
            AddSystem(new TrajectorySystem());
        }
    }
}