using System;
using Leopotam.Ecs;
using Massive.Gameplay.Components;
using Massive.Gameplay.Markers;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class EnemySpawnPointsInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _world = null;
        
        public void Initialize()
        {
            var points = Object.FindObjectsOfType<EnemySpawnPointMarker>();

            foreach (var point in points)
            {
                var spawnPoint = _world.CreateEntityWith<EnemySpawnPoint>();
                spawnPoint.Position = point.transform.position;
                spawnPoint.Rotation = point.transform.rotation;
                spawnPoint.LastSpawnTime = Time.time;
            }
        }

        public void Destroy()
        {
            
        }
    }
}