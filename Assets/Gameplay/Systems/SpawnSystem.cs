using System;
using Leopotam.Ecs;
using Massive.Gameplay.Components;
using Massive.Gameplay.GameResources;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class SpawnSystem : IEcsRunSystem
    {
        private readonly EcsWorld _world = null;
        private readonly EcsFilter<EnemySpawnPoint> _spawnFilter = null;
        private readonly EcsFilter<PlayerShip> _playerFilter = null;

        private readonly IResourceProvider _resourceProvider;

        public SpawnSystem(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
        }

        public void Run()
        {
            for (var i = 0; i < _spawnFilter.EntitiesCount; i++)
            {
                var spawnPoint = _spawnFilter.Components1[i];
                if (Time.time - spawnPoint.LastSpawnTime >= 10)
                {
                    spawnPoint.LastSpawnTime = Time.time;
                    var enemyShipResource =
                        _resourceProvider.GetResource<GameObject>("EnemyShips/StarSparrow" + Random.Range(1, 9));
                    var enemyGameObject =
                        Object.Instantiate(enemyShipResource, spawnPoint.Position, spawnPoint.Rotation);

                    EnemyShip enemyShip;
                    Movable movable;
                    Trajectory trajectory;
                    _world.CreateEntityWith(out enemyShip, out movable, out trajectory);

                    enemyShip.Transform = enemyGameObject.GetComponent<Transform>();
                    movable.Rigidbody = enemyGameObject.GetComponent<Rigidbody>();

                    int closestPlayer = 0;
                    for (int j = 0; j < _playerFilter.EntitiesCount; j++)
                    {
                        float dist1 = Vector3.Distance(_playerFilter.Components1[closestPlayer].Transform.position,
                            enemyShip.Transform.position);
                        float dist2 = Vector3.Distance(_playerFilter.Components1[j].Transform.position,
                            enemyShip.Transform.position);
                        if (dist2 < dist1)
                        {
                            closestPlayer = j;
                        }
                    }

                    Vector3 closestPlayerPosition = _playerFilter.Components1[closestPlayer].Transform.position;

                    trajectory.Points = GetRandomPathBetweenPoints(spawnPoint.Position, closestPlayerPosition);
                }
            }
        }

        private Vector3[] GetRandomPathBetweenPoints(Vector3 start, Vector3 end)
        {
            return new[]
            {
                start,
                Vector3.Lerp(start, end, 0.25f) + Random.insideUnitSphere * 10,
                Vector3.Lerp(start, end, 0.5f) + Random.insideUnitSphere * 10,
                Vector3.Lerp(start, end, 0.75f) + Random.insideUnitSphere * 10,
                end
            };
        }
    }
}