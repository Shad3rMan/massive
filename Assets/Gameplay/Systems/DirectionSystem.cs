using System;
using Leopotam.Ecs;
using Leopotam.Ecs.Threads;
using Massive.Gameplay.Components;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class DirectionSystem : EcsMultiThreadSystem<EcsFilter<Movable, Trajectory>>
    {
        private readonly EcsFilter<Movable, Trajectory> _movableFilter = null;

        protected override EcsFilter<Movable, Trajectory> GetFilter()
        {
            return _movableFilter;
        }

        protected override EcsMultiThreadWorker GetWorker()
        {
            return WorkerThread;
        }

        protected override int GetMinJobSize()
        {
            return 15;
        }

        protected override int GetThreadsCount()
        {
            return Environment.ProcessorCount - 1;
        }

        private static void WorkerThread(EcsFilter<Movable, Trajectory> filter, int from, int to)
        {
            for (int i = from, iMax = to; i < iMax; i++)
            {
                var movable = filter.Components1[i];
                var trajectory = filter.Components2[i];
                var target = trajectory.Points[trajectory.Points.Length - 1];

                movable.Direction = (target - movable.Position).normalized;
            }
        }

    }
}