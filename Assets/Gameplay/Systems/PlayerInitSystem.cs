using Leopotam.Ecs;
using Massive.Gameplay.Components;
using UnityEngine;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class PlayerInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _world = null;

        public void Initialize()
        {
            var ship = GameObject.Find("PlayerShip");
            _world.CreateEntityWith<PlayerShip>().Transform = ship.transform;
        }

        public void Destroy()
        {
        }
    }
}