using Leopotam.Ecs;
using Massive.Gameplay.Components;
using UnityEngine;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class TrajectoryGizmosSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Movable, Trajectory> _trajectoryFilter = null;
        
        public void Run()
        {
            for (int i = 0; i < _trajectoryFilter.EntitiesCount; i++)
            {
                var movable = _trajectoryFilter.Components1[i];
                var trajectory = _trajectoryFilter.Components2[i];

                for (int j = 0; j < trajectory.Points.Length - 1; j++)
                {
                    Gizmos.color = Color.cyan;
                    Gizmos.DrawLine(trajectory.Points[j], trajectory.Points[j + 1]);
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine(movable.Position, trajectory.CurrentTarget);
                }
            }
        }
    }
}