using Leopotam.Ecs;
using Massive.Gameplay.Components;
using UnityEngine;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class MovableOrientationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Movable> _movableFilter = null;
        
        public void Run()
        {
            for (int i = 0; i < _movableFilter.EntitiesCount; i++)
            {
                var movable = _movableFilter.Components1[i];

                movable.Rigidbody.transform.rotation = Quaternion.LookRotation(movable.Direction); // Bad to access transfrom from here
            }
        }
    }
}