using Leopotam.Ecs;
using Massive.Gameplay.Components;
using UnityEngine;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class SpeedSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Movable, Trajectory> _movableFilter = null;

        public void Run()
        {
            for (int i = 0; i < _movableFilter.EntitiesCount; i++)
            {
                var movable = _movableFilter.Components1[i];
                var trajectory = _movableFilter.Components2[i];

                movable.Speed = Mathf.Sqrt(Vector3.Distance(movable.Rigidbody.position, trajectory.CurrentTarget));
            }
        }
    }
}