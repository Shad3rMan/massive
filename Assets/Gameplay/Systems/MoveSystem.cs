using Leopotam.Ecs;
using Massive.Gameplay.Components;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class MoveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Movable> _movablesFilter = null;

        public void Run()
        {
            for (int i = 0; i < _movablesFilter.EntitiesCount; i++)
            {
                var movable = _movablesFilter.Components1[i];

                movable.Rigidbody.AddForce(movable.Direction * movable.Speed);
                movable.Position = movable.Rigidbody.position;
            }
        }
    }
}