using System;
using Leopotam.Ecs;
using Leopotam.Ecs.Threads;
using Massive.Gameplay.Components;
using UnityEngine;

namespace Massive.Gameplay.Systems
{
    [EcsInject]
    public class TrajectorySystem : EcsMultiThreadSystem<EcsFilter<Movable, Trajectory>>
    {
        private readonly EcsFilter<Movable, Trajectory> _movableFilter = null;

        protected override EcsFilter<Movable, Trajectory> GetFilter()
        {
            return _movableFilter;
        }

        protected override EcsMultiThreadWorker GetWorker()
        {
            return WorkerThread;
        }

        protected override int GetMinJobSize()
        {
            return 15;
        }

        protected override int GetThreadsCount()
        {
            return Environment.ProcessorCount - 1;
        }

        private void WorkerThread(EcsFilter<Movable, Trajectory> filter, int from, int to)
        {
            for (int i = from, iMax = to; i < iMax; i++)
            {
                var movable = filter.Components1[i];
                var trajectory = filter.Components2[i];

                trajectory.CurrentTarget = GetNextTarget(trajectory.Points, movable.Position);
            }
        }

        private static Vector3 GetNextTarget(Vector3[] points, Vector3 position)
        {
            int resultPointIndex = 0;
            for (int i = 0; i < points.Length; i++)
            {
                if (Vector3.Distance(position, points[i]) < Vector3.Distance(position, points[resultPointIndex]))
                {
                    resultPointIndex = i;
                }
            }
            
            resultPointIndex = Mathf.Clamp(resultPointIndex + 1, 0, points.Length - 1);

            return points[resultPointIndex];
        }

        private static Vector3 Lerp(Vector3[] points, float t)
        {
            int numSections = points.Length - 3;
            int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
            float u = t * numSections - currPt;
            Vector3 a = points[currPt];
            Vector3 b = points[currPt + 1];
            Vector3 c = points[currPt + 2];
            Vector3 d = points[currPt + 3];
            return .5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) +
                          (-a + c) * u + 2f * b);
        }
    }
}