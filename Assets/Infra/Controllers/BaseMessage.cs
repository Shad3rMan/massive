namespace Infra.Controllers
{
    public class BaseMessage
    {
        public bool Processed { get; set; }
    }
}