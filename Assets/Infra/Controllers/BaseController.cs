﻿using System.Collections.Generic;

namespace Infra.Controllers
{
    public abstract class BaseController
    {
        private readonly IControllerFactory _controllerFactory;
        private readonly List<BaseController> _children;

        private BaseController _parent;

        protected BaseController(IControllerFactory controllerFactory)
        {
            _controllerFactory = controllerFactory;
            _children = new List<BaseController>();
        }

        private void Start()
        {
            foreach (var child in _children)
            {
                child.Start();
            }

            OnStart();
        }

        private void Stop()
        {
            foreach (var child in _children)
            {
                child.Stop();
            }

            OnStop();
        }

        protected virtual void OnStart()
        {
        }

        protected virtual void OnStop()
        {
        }

        private void AddChildController(BaseController controller)
        {
            _children.Add(controller);
            controller._parent = this;
            controller.Start();
        }

        protected T AddChildController<T>() where T : BaseController
        {
            var controller = _controllerFactory.CreateController<T>();
            AddChildController(controller);
            return controller;
        }

        protected void RemoveChildController(BaseController controller)
        {
            if (_children.Contains(controller))
            {
                _children.Remove(controller);
                controller.Stop();
            }
        }

        protected abstract bool HandleMessage(BaseMessage message);

        protected void SendMessageUp(BaseMessage message)
        {
            if (!HandleMessage(message))
            {
                _parent.SendMessageUp(message);
            }
        }

        protected void SendMessageDown(BaseMessage message)
        {
            foreach (var child in _children)
            {
                if (!child.HandleMessage(message))
                {
                    child.SendMessageDown(message);
                }
            }
        }
    }
}